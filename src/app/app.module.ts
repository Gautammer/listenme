import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { LoadingController } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GenderPage } from '../pages/gender/gender';
import { BoyPage } from '../pages/boy/boy';
import { GirlPage } from '../pages/girl/girl';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    GenderPage,
    BoyPage,
    GirlPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GenderPage,
    BoyPage,
    GirlPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    

    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
