import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BoyPage } from '../boy/boy';
import { GirlPage } from '../girl/girl';

@IonicPage()
@Component({
  selector: 'page-gender',
  templateUrl: 'gender.html',
})
export class GenderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  male(){
    this.navCtrl.push(BoyPage);

  }
  female(){
    this.navCtrl.push(GirlPage);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenderPage');
  }

}
