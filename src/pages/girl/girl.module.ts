import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GirlPage } from './girl';

@NgModule({
  declarations: [
    GirlPage,
  ],
  imports: [
    IonicPageModule.forChild(GirlPage),
  ],
})
export class GirlPageModule {}
