import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { GenderPage } from '../gender/gender';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController) {

  }

    gender(){
      let loader = this.loadingCtrl.create({
        spinner:"dots",
        content: "Please wait...",
        duration: 1000
      });
      loader.present();
      this.navCtrl.push(GenderPage);
    }
    loading() {
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 2000
      });
      loader.present();
   }

}
